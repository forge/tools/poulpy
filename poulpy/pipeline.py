from io import BytesIO
import os
from pathlib import Path
import platform
import rich
import shutil
import stat
import subprocess
import tarfile
from typing import IO, List, Optional

from requests import Response
import requests

import poulpy
from poulpy.config import Config
from poulpy.restish import Restish


class Pipeline:
    pipeline: str
    restish: Restish

    version: str = "0.75.0"
    machine: str = platform.machine().lower()
    system: str = platform.system().lower()
    url: str = (
        "https://github.com/nushell/nushell/releases/download/{version}/nu-{version}-{machine}-{system}.{extension}"
    )

    nushell_path: Path = Config.nushell_binary

    def __init__(self, pipeline: str):
        """Install the Nushell depending on the OS and computer architecture.

        Args:
            pipeline (str): the command to pass to nushell.
        """
        self.restish = Restish()
        self.restish.update()
        self.pipeline = pipeline

        if shutil.which("nu") is not None:
            self.nushell_path = Path("nu")
            return

        if self.nushell_path.exists():
            os.environ["PATH"] += os.pathsep + str(self.nushell_path.parent)
            return

        url: str = self._get_download_url()
        response: Response = requests.get(url, stream=True)

        if response.status_code != 200:
            raise EnvironmentError(
                f"""
Failed to download nushell shell from the following link: {url}

You can install it manually here: https://github.com/nushell/nushell#installation.
However, the `nu` binary must be placed here: '{self.nushell_path}'.
            """
            )

        file_path: str = f"nu-{self.version}-{self.machine}-{self.system}/nu"
        with tarfile.open(fileobj=BytesIO(response.content)) as file:
            file_reader: Optional[IO[bytes]] = file.extractfile(file_path)
            file_content: Optional[bytes] = file_reader.read() if file_reader is not None else None

        if file_content is None:
            raise EnvironmentError("Failed to extract the Nushell tarball")

        self.nushell_path.write_bytes(file_content)
        os.chmod(self.nushell_path, stat.S_IRWXO | stat.S_IRWXU | stat.S_IRWXG)
        os.environ["PATH"] += os.pathsep + str(self.nushell_path.parent)

    def _get_download_url(self) -> str:
        """Get the nushell download url depending on the OS and computer architecture."""

        extension: str = "tar.gz"

        if self.system == "linux":
            self.system = "unknown-linux-gnu"
        elif self.system == "windows":
            self.system = "pc-windows-msvc"
            extension = "zip"
        elif self.system == "mac":
            self.system = "apple-darwin"
        elif self.system == "darwin":
            self.system = "apple-darwin"
        else:
            raise EnvironmentError(f"Nushell cannot be download for the following self.system: {self.system}")

        return self.url.format(version=self.version, machine=self.machine, system=self.system, extension=extension)

    def _sanitity_check(self):
        command: List[str] = [str(self.nushell_path), "--version"]
        process: subprocess.CompletedProcess = subprocess.run(command, stdout=subprocess.PIPE)
        version: str = process.stdout.decode().strip()
        if version != self.version:
            rich.print(
                f"[yellow] Nushell version is not the expected one. Found: {version}, expected: {self.version}[/yellow]",
            )

    def run(self, profile_name: str, **subprocess_args) -> subprocess.CompletedProcess:
        """Call nushell with the good command in a subprocess.

        if a poulpy service is detected in the command, automatically add the auth flag and call the `poulpy` command.

        Args:
            profile_name (str): name of the auth profile to use for poulpy commands.
            subprocess_args: additional arguments to pass to the subprocess.

        Returns:
            CompletedProcess: the subprocess that called nushell.
        """
        self._sanitity_check()

        for server in poulpy.servers:
            self.pipeline = self.pipeline.replace(server, f"poulpy {server} -p {profile_name}")

        return subprocess.run([str(self.nushell_path), "-c", self.pipeline], **subprocess_args)
