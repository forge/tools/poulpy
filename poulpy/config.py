import json
import shutil
from enum import Enum
from typing import Dict
from pathlib import Path

from pkg_resources import resource_filename


class Config:
    class Type(Enum):
        ALL = 1
        RESTISH = 2
        PRESETS = 3

    poulpy_folder: Path = Path.home() / ".poulpy"

    restish_config: Path = poulpy_folder / "restish.json"
    presets_config: Path = poulpy_folder / "presets.json"
    scripts_folder: Path = poulpy_folder / "nu-scripts"

    restish_binary: Path = poulpy_folder / "restish"
    nushell_binary: Path = poulpy_folder / "nu"

    # Json object of poupy config file
    restish: Dict
    presets: Dict

    def __new__(cls):
        """
        Implements the singleton pattern.
        """
        if not hasattr(cls, "instance"):
            cls.instance = super(Config, cls).__new__(cls)
        return cls.instance

    def __init__(self):
        """
        Create poulpy folder and configuration file if they don't exist.
        """
        self.poulpy_folder.mkdir(exist_ok=True, parents=True)

        self._import_config("default_restish_config.json", self.restish_config)
        self._import_config("default_presets_config.json", self.presets_config)
        self._import_config("nu-scripts", self.scripts_folder, is_dir=True)

        self.restish = self._load_json(self.restish_config)
        self.presets = self._load_json(self.presets_config)

    @staticmethod
    def _load_json(path: Path) -> Dict:
        with open(path) as file:
            return json.load(file)

    @staticmethod
    def _get_resource_path(filename: str) -> Path:
        filepath: Path = Path("resources") / filename
        path: str = resource_filename("poulpy", str(filepath))
        return Path(path)

    @staticmethod
    def _import_config(filename: str, destination: Path, is_dir: bool = False, exist_ok: bool = False):
        if destination.exists() and not exist_ok:
            return

        if is_dir:
            shutil.copytree(Config._get_resource_path(filename), destination, dirs_exist_ok=True)
        else:
            shutil.copyfile(Config._get_resource_path(filename), destination)

    def refresh(self, type: Type):
        if type == Config.Type.ALL or type == Config.Type.PRESETS:
            default_presets = self._load_json(self._get_resource_path("default_presets_config.json"))

            for key, value in default_presets.items():
                self.presets[key] = value

            self.nushell_binary.unlink(missing_ok=True)
            json.dump(self.presets, open(self.presets_config, "w"), indent=4)
            self._import_config("nu-scripts", self.scripts_folder, is_dir=True, exist_ok=True)

        if type == Config.Type.ALL or type == Config.Type.RESTISH:
            default_restish = self._load_json(self._get_resource_path("default_restish_config.json"))

            for key, value in default_restish.items():
                self.restish[key] = value

            self.restish_binary.unlink(missing_ok=True)
            json.dump(self.restish, open(self.restish_config, "w"), indent=4)
