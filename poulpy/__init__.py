import sys
from typing import Callable, Dict, Tuple

import click
import rich
import jsonschema

from poulpy.auth import Credentials, DefaultProfiles, get_credentials
from poulpy.config import Config
from poulpy.intranet.pacv2 import PaCV2
from poulpy.intranet.tenant import Tenant
from poulpy.pipeline import Pipeline
from poulpy.restish import Restish

__default_auth_profile = DefaultProfiles.prod.name

from rich.console import Console
from rich.traceback import install

console = Console()
install(suppress=[click, jsonschema])

try:
    import click_completion

    click_completion.init()
except ImportError:
    pass


@click.group()
def main():
    """
    Main entry point for the application.
    """
    pass


@main.command(help="Get poulpy version")
def version():
    from importlib import metadata

    print(f'Poulpy version: {metadata.version("poulpy")}')
    print(f"Nushell version: {Pipeline.version}")
    print(f"Restish version: {Restish.version}")


@main.group(help="Intranet validation and deployment commands")
def intranet():
    pass


@intranet.command(help="Validate a PaCV2 metadata file")
@click.option("--metadata", required=True, type=click.Path(exists=True, readable=True))
@click.option("--base-url", envvar="REPO_ACTIVITY_BASE_URL", default="https://repo-activity.api.forge.epita.fr")
def pacv2_validate(metadata: str, base_url: str):
    pacv2 = PaCV2(metadata, base_url)
    rich.print_json(data=pacv2.validate())


@intranet.command(help="Deploy a PaCV2 activity")
@click.option("--metadata", required=True, type=click.Path(exists=True, readable=True))
@click.option("--base-url", envvar="REPO_ACTIVITY_BASE_URL", default="https://repo-activity.api.forge.epita.fr")
@click.option("--token", prompt=True, hide_input=True, envvar="ACTIVITY_TOKEN")
def pacv2_deploy(metadata: str, token: str, base_url: str):
    if not token:
        raise ValueError("Token is required")

    pacv2 = PaCV2(metadata, base_url)
    pacv2.deploy(token)


@intranet.command(help="Validate a Tenant metadata file")
@click.option("--metadata", required=True, type=click.Path(exists=True, readable=True))
@click.option("--base-url", envvar="TENANT_ACTIVITY_BASE_URL", default="https://repo-tenant.api.forge.epita.fr")
def tenant_validate(metadata: str, base_url: str):
    tenant = Tenant(metadata, base_url)
    rich.print_json(data=tenant.validate())


@intranet.command(help="Deploy a Tenant activity")
@click.option("--metadata", required=True, type=click.Path(exists=True, readable=True))
@click.option("--base-url", envvar="TENANT_ACTIVITY_BASE_URL", default="https://repo-tenant.api.forge.epita.fr")
@click.option("--token", prompt=True, hide_input=True, envvar="TENANT_TOKEN")
def tenant_deploy(metadata: str, token: str, base_url: str):
    if not token:
        raise ValueError("Token is required")

    tenant = Tenant(metadata, base_url)
    tenant.deploy(token)


@main.command()
@click.option("--debug/--no-debug", default=False, help="Enable debug output.")
@click.option("--quiet/--no-quiet", default=False, help="Do not print the retrieved token.")
@click.option(
    "-p",
    "--profile",
    default=__default_auth_profile,
    type=click.Choice([str(i) for i in DefaultProfiles.__members__]),
    help="Authentication profile, i.e. the environment you should connect to.",
)
def auth(debug: bool, quiet: bool, profile: str):
    """
    Authentication tool for getting a JWT that can be used to authenticate against LabSI APIs.

    This utility is designed to be piped to other tools. As such, all log messages are sent to stderr and only the token is sent to stdout.

    Several profiles are supported depending on what services you want to connect to (e.g. a local service, a production service, a staging service...).
    """

    actual_profile = DefaultProfiles[profile].value

    credentials: Credentials = get_credentials(actual_profile)

    if not quiet:
        print(credentials.jwt)


@main.command()
@click.option(
    "-t",
    "--type",
    default=Config.Type.ALL.name,
    type=click.Choice([str(i) for i in Config.Type.__members__]),
    help="The type of configuration to refresh.",
)
def refresh(type: str):
    """
    Refresh poulpy default configuration files. You can specify the type of configuration to refresh.
    """
    Config().refresh(Config.Type[type])


@main.command(context_settings=dict(ignore_unknown_options=True, allow_extra_args=True))
@click.argument("pipeline", type=str)
@click.option(
    "-p",
    "--profile",
    type=click.Choice([str(i) for i in DefaultProfiles.__members__]),
    help="Authentication profile, i.e. the environment you should connect to.",
    default=__default_auth_profile,
)
def pipe(pipeline: str, profile: str):
    """
    Tool used to cascade the output of one poulpy command to the input of another.
    It only works with the commands calling Forge services.
    """
    Pipeline(pipeline).run(profile)


def restish_service(name: str) -> Callable:
    """
    'Templated' function used when calling services defined in the config file.
    """

    def inner(args: Tuple[str, ...], profile: str, help: bool):
        actual_profile = DefaultProfiles[profile].value
        restish: Restish = Restish()
        restish.update()

        if help:
            args = (*args, "--help")

        stdin: bytes = sys.stdin.buffer.read() if not sys.stdin.isatty() else b""
        restish.run(actual_profile, [name, *args], input=stdin)

    inner.__name__ = name

    return inner


# Dict containing all Click command that calls compatible Forge services.
servers: Dict[str, click.Command] = {}

# Dynamically create Click Command for Forge services with the 'templated' function defined above.
for service in Config().restish.keys():
    cmd = main.command(
        name=service,
        help=f"Rest Client of {service}",
        context_settings=dict(ignore_unknown_options=True, allow_extra_args=True),
    )(restish_service(service))
    click.argument("args", nargs=-1, type=click.UNPROCESSED)(cmd)
    click.option(
        "-p",
        "--profile",
        type=click.Choice([str(i) for i in DefaultProfiles.__members__]),
        help="Authentication profile, i.e. the environment you should connect to.",
        default=__default_auth_profile,
    )(cmd)

    # We must specify --help or it will be captured by click
    click.option("--help", is_flag=True, help="Show this message and exit.")(cmd)

    servers[service] = cmd


def preset_command(name: str) -> Callable:
    """
    'Templated' function used when calling presets commands.
    """

    def inner(**kwargs):
        command: str = Config().presets[name]["cmd"]
        profile: str = kwargs["profile"]

        try:
            command = command.format(**kwargs)
        except Exception:
            raise ValueError(f"Invalid preset configuration: Failed to format the command: {command} with: {kwargs}")

        Pipeline(command).run(profile)

    inner.__name__ = name

    return inner


# Dynamically create Click Command for presets commands with the 'templated' function defined above.
for preset in Config().presets.keys():
    # The configuration must at least define
    if "cmd" not in Config().presets[preset]:
        raise ValueError("Invalid preset configuration: the 'cmd' field is not defined")

    cmd = main.command(name=preset, help=Config().presets[preset].get("description", ""))(preset_command(preset))

    for argument in Config().presets[preset].get("args", []):
        click.option(
            f"--{argument}", type=str, required=True, help="String argument that will be injected in the pipe command."
        )(cmd)

    click.option(
        "-p",
        "--profile",
        type=click.Choice([str(i) for i in DefaultProfiles.__members__]),
        help="Authentication profile, i.e. the environment you should connect to.",
        default=__default_auth_profile,
    )(cmd)
