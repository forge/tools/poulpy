from io import BytesIO
from pathlib import Path
import platform
import rich
import shutil
import subprocess
import tarfile
from typing import List
from typing import List
import urllib.parse

import requests
from requests import Response

from poulpy.auth import AuthConfiguration, get_credentials
from poulpy.config import Config


class Restish:
    # Get this value from a proper config file once we have one
    version: str = "0.15.2"
    system: str = platform.system().lower()
    machine: str = platform.machine().lower()

    url: str = f"""https://github.com/danielgtaylor/restish/releases/download/v{version}/restish-{version}-{system}-{machine}.tar.gz"""

    restish_binary: Path = Config.restish_binary

    def __init__(self):
        """
        Install restish if it is not already installed.
        Raise an error if the desired restish version do not exists.
        """
        if shutil.which("restish") is not None:
            self.restish_binary = Path("restish")
            return

        if self.restish_binary.exists():
            return

        response: Response = requests.get(self.url, stream=True)

        if response.status_code != 200:
            raise EnvironmentError(f"Failed to download restish from the following link: {self.url}")

        with tarfile.open(fileobj=BytesIO(response.content)) as file:
            file.extract("restish", self.restish_binary.parent)

    @staticmethod
    def _skip_options(args: List[str], i: int = 0) -> int:
        """
        Return the index of the first argument that is not an option.
        """
        # We skip all the options
        while i < len(args) and args[i].startswith("-"):
            i += (
                2
                if args[i]
                in (
                    "-o",
                    "--rsh-output-format",
                    "-H",
                    "--rsh-header",
                    "-f",
                    "--rsh-filter",
                    "-p",
                    "--rsh-profile",
                    "-s",
                    "--rsh-server",
                    "-q",
                    "--rsh-query",
                    "--rsh-ca-cert",
                    "--rsh-client-cert",
                    "--rsh-client-key",
                )
                else 1
            )

        return i

    @staticmethod
    def _quote_args(args: List[str]) -> List[str]:
        """
        The quoting is very tricky since we don't know what arguments restish can take.

        We can't just quote everything since some arguments are used by restish itself and not passed in the api call,
        some arguments passed by the api call don't need to be quoted since they are not in the url.

        Fortunately restish makes path parameters into CLI positional arguments while the rest are passed as CLI options.
        We need to detect which arguments are path parameters, options or API's name.
        """
        args = list(args)

        i = Restish._skip_options(args)
        # No positional arguments: this is not a api call
        if i >= len(args):
            return args

        # We skip the name of the api we call and we skip the options
        i = Restish._skip_options(args, i + 1)

        # Only one positional argument: this is not a api call
        if i >= len(args):
            return args

        # We skip the name of the method we call and we skip the options
        i = Restish._skip_options(args, i + 1)

        # All the next positional arguments are path parameters, we need to quote them
        if i < len(args) and not args[i].startswith("-"):
            args[i] = urllib.parse.quote(args[i], safe="")

        return args

    def _sanitity_check(self):
        command: List[str] = [str(self.restish_binary), "--version"]
        process: subprocess.CompletedProcess = subprocess.run(command, stdout=subprocess.PIPE)
        version: str = process.stdout.decode().strip()[16:]
        if version != self.version:
            rich.print(
                f"[yellow] Restish version is not the expected one. Found: {version}, expected: {self.version}[/yellow]",
            )

    def update(self):
        """
        Update Restish config with current poulpy config.
        """
        dst: Path = Path.home() / ".restish" / "apis.json"
        dst.parent.mkdir(parents=True, exist_ok=True)
        shutil.copyfile(Config.restish_config, dst)

    def run(self, profile: AuthConfiguration, args: List[str], **subprocess_args) -> subprocess.CompletedProcess:
        """
        Run restish with the given arg and the authentification header corresponding to the given profile.

        Args:
            profile: which profile to use for authorization.
            args: arguments to pass to restish
            subprocess_args: additional parameters passed to the subprocess.

        Returns:
            The subprocess corresponding to the restish call.
        """
        self._sanitity_check()

        command: List[str] = [
            str(self.restish_binary),
            *self._quote_args(args),
            "-H",
            f"Authorization: Bearer {get_credentials(profile).jwt}",
        ]

        if "-v" in command or "--rsh-verbose" in command:
            cmd: str = " ".join(arg for arg in command[:-2])
            print(f"\nThe command you tried to run (without the authentification header):\n{cmd}\n")

        return subprocess.run(command, **subprocess_args)
