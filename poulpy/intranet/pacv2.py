import hashlib
import json
import os
from typing import Dict, Iterator, Optional
import jsonschema
import requests
import rich
from rich.progress import Progress, TimeElapsedColumn, SpinnerColumn
import magic

from poulpy.config import Config
from poulpy.intranet import yaml


def hash_file(filepath: str) -> str:
    if not os.path.isfile(filepath):
        raise FileNotFoundError(f"Document {filepath} not found or is not a file.")

    h = hashlib.md5()
    b = bytearray(os.stat(filepath).st_size)
    mv = memoryview(b)
    with open(filepath, "rb", buffering=0) as f:
        while n := f.readinto(mv):
            h.update(mv[:n])
    return h.hexdigest()


def setdefault_lambda(d: Dict, key: str, default):
    if key not in d:
        d[key] = default()

    return d[key]


class PaCV2:

    post_url: str
    schema_url: str
    metadata_path: str
    config: Config
    json_schema: Dict
    activity: Dict

    def __init__(self, metadata_path: str, base_url: str) -> None:

        # Will be formatted with activity slug at the post request
        self.post_url = base_url + "/activity/{activitySlug}"
        self.schema_url = base_url + "/activity/schema"
        self.config: Config = Config()
        self.metadata_path = metadata_path
        self.json_schema = requests.get(self.schema_url).json()
        self.activity = self.load_metadata()

    def load_metadata(self) -> Dict:
        """
        Open metadata file and evaluate it
        """
        try:
            with open(self.metadata_path) as file:
                activity = yaml.load(file)
        except FileNotFoundError as e:
            print("No activity metadata found at path: " + self.metadata_path)
            raise e
        except ValueError as e:
            print("Error while parsing activity metadata at path: " + self.metadata_path)
            raise e

        return self.add_versions(activity)

    def get_documents(self, activity: Dict) -> Iterator:
        """
        Return all document objects in the activity metadata.
        We have to do some manual validation because this set happens before the schema validation.
        """

        def __get_documents(parent_slug: str, dict: Optional[Dict]) -> Iterator:
            if dict is None:
                return

            if "slug" not in dict:
                raise ValueError(
                    f"No slug provided: every assignment and assignment group must have a slug, please check your {self.metadata_path} file. (Current context: "
                    + parent_slug
                    + ")"
                )

            slug = f"{parent_slug}/{dict['slug']}"

            if assignments := dict.get("assignments"):
                for assignment in assignments:
                    yield from __get_documents(slug, assignment)

            if assignmentGroups := dict.get("assignmentGroups"):
                for assignmentGroup in assignmentGroups:
                    yield from __get_documents(slug, assignmentGroup)

            if documents := dict.get("documents"):
                for document in documents:
                    if "filename" not in document:
                        raise ValueError(
                            f"No filename provided: every document please check your {self.metadata_path} file. (Current context: "
                            + slug
                            + ")"
                        )

                    yield f"{slug}/{document['filename']}", document

        if "assignments" in activity:
            for assignment in activity["assignments"]:
                yield from __get_documents("root", assignment)

        if "assignmentGroups" in activity:
            for assignmentGroup in activity["assignmentGroups"]:
                yield from __get_documents("root", assignmentGroup)

    def add_versions(self, activity: Dict) -> Dict:
        """
        Add versions values if they are not already present in all the activity metadata.
        We have to do some manual validation because this set happens before the schema validation.
        """
        setdefault_lambda(activity, "versionHash", lambda: hash_file(self.metadata_path))

        for _, document in self.get_documents(activity):
            if document.get("type") == "EXTERNAL":
                if "version" not in document:
                    raise ValueError(
                        "Versions for external documents must be provided manually: no version provided for "
                        + document["filename"]
                    )

                continue

            if "path" not in document:
                raise ValueError(
                    f"No path provided: every document must have a path except external documents, please check your {self.metadata_path} file."
                )

            setdefault_lambda(document, "version", lambda: hash_file(document["path"]))

        return activity

    def validate(self) -> Dict:
        """
        Validate the activity metadata against the schema.
        """

        try:
            jsonschema.validate(self.activity, self.json_schema)
        except jsonschema.ValidationError as e:
            print(
                f"Please check your {self.metadata_path} file: error while validating activity metadata against schema"
            )
            raise e

        return self.activity

    def deploy(self, token: str) -> None:

        post_url = self.post_url.format(activitySlug=f"{self.activity['slug']}")

        rich.print(f"Deploying file {self.metadata_path} to {post_url}")

        self.validate()

        resp = requests.put(
            post_url,
            params={"activityToken": token},
            data=json.dumps(self.activity),
            headers={"Content-Type": "text/plain"},
            timeout=3000,
            stream=False,
        )

        if 400 <= resp.status_code < 500:
            raise Exception(
                f"Please check your {self.metadata_path} file: error while deploying activity: " + resp.text
            )

        if 500 <= resp.status_code:
            raise Exception(f"Server error while deploying activity: " + resp.text)

        nb_docs = len(resp.json()["presignedUrls"])

        rich.print("Activity deployed successfully")

        rich.print(f"Uploading {nb_docs} documents...")

        progress = Progress(
            SpinnerColumn(),
            *Progress.get_default_columns(),
            TimeElapsedColumn(),
        )

        task = progress.add_task("Documents", total=nb_docs)

        progress.__enter__()

        presigned_urls = resp.json()["presignedUrls"]
        documents = self.get_documents(self.activity)
        for path, document in documents:
            if document.get("type") == "EXTERNAL":
                continue

            if path not in presigned_urls:
                progress.__exit__(None, None, None)
                raise ValueError("No presigned url found for document: " + path)

            mime_type = magic.from_file(document["path"], mime=True)

            with open(document["path"], "rb") as f:
                resp = requests.put(presigned_urls[path], data=f, timeout=10, headers={"Content-Type": mime_type})

            if resp.status_code != 200:
                progress.__exit__(None, None, None)
                raise Exception("Error while deploying document: " + resp.text)

            progress.update(task, advance=1)

        progress.__exit__(None, None, None)
        rich.print(f"{nb_docs} documents uploaded successfully")
