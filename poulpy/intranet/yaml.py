import functools
import json
import os
from typing import IO, Any
import warnings
import toml
import yaml

__all__ = ["load"]


class ExtLoaderMeta(type):
    def __new__(metacls, __name__, __bases__, __dict__):
        """Add include constructer to class."""

        # register the include constructor on the class
        cls = super().__new__(metacls, __name__, __bases__, __dict__)
        cls.add_constructor("!include", cls.construct_include)

        return cls


class ExtLoader(yaml.Loader, metaclass=ExtLoaderMeta):
    """YAML Loader with `!include` constructor."""

    def __init__(self, stream: IO):
        """Initialise Loader."""

        try:
            self._root = os.path.split(stream.name)[0] if os.environ.get("RELATIVE_INCLUDE") else os.getcwd()
        except AttributeError:
            warnings.warn(
                "Cannot determine root path for !include constructor. Choosing current directory.", RuntimeWarning
            )
            self._root = os.getcwd()

        super().__init__(stream)

    def construct_include(self, node: yaml.Node) -> Any:
        """Include file referenced at node."""

        if not isinstance(node, yaml.ScalarNode):
            raise yaml.constructor.ConstructorError(
                None,
                None,
                f"expected a string node, but found {type(node).__name__}",
                node.start_mark,
            )

        filename = os.path.abspath(os.path.join(self._root, str(self.construct_scalar(node))))
        extension = os.path.splitext(filename)[1].lstrip(".")

        try:
            with open(filename, "r") as f:
                if extension in ("yaml", "yml"):
                    return yaml.load(f, ExtLoader)
                elif extension in ("json",):
                    return json.load(f)
                elif extension in ("toml",):
                    return toml.load(f)
                else:
                    warnings.warn(
                        f"File extension used as argument for !include is unknown, parsing as text", RuntimeWarning
                    )
                    return "".join(f.readlines())
        except yaml.YAMLError as e:
            raise yaml.constructor.ConstructorError(
                None, None, f"Error during parsing of yaml of '!include': YAMLError({filename}", node.start_mark
            ) from e
        except toml.TomlDecodeError as e:
            raise yaml.constructor.ConstructorError(
                None, None, f"Error during parsing of toml of '!include': TomlDecodeError({filename}", node.start_mark
            ) from e
        except json.JSONDecodeError as e:
            raise yaml.constructor.ConstructorError(
                None, None, f"Error during parsing of json of '!include': JSONDecodeError({filename}", node.start_mark
            ) from e
        except OSError as e:
            raise yaml.constructor.ConstructorError(
                None, None, f"Error during execution of '!include': OSError({filename}", node.start_mark
            ) from e


def load(*args, **kwargs):
    """Load YAML with ExtLoader."""

    kwargs.setdefault("Loader", ExtLoader)
    return yaml.load(*args, **kwargs)
