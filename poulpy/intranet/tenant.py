import json
from typing import Dict
import jsonschema
import requests
import rich

from poulpy.config import Config
from poulpy.intranet import yaml


class Tenant:

    post_url: str
    schema_url: str
    metadata_path: str
    config: Config
    json_schema: Dict
    tenant: Dict

    def __init__(self, metadata_path: str, base_url: str) -> None:

        # Will be formatted with activity slug at the post request
        self.post_url = base_url + "/tenant"
        self.schema_url = base_url + "/tenant/schema"
        self.config: Config = Config()
        self.metadata_path = metadata_path
        self.json_schema = requests.get(self.schema_url).json()
        self.tenant = self.load_metadata()

    def load_metadata(self) -> Dict:
        """
        Open metadata file and evaluate it
        """
        try:
            with open(self.metadata_path) as file:
                activity = yaml.load(file)
        except FileNotFoundError as e:
            print("No tenant metadata found at path: " + self.metadata_path)
            raise e
        except ValueError as e:
            print("Error while parsing tenant metadata at path: " + self.metadata_path)
            raise e

        return activity

    def validate(self) -> Dict:
        """
        Validate the activity metadata against the schema.
        """

        try:
            jsonschema.validate(self.tenant, self.json_schema)
        except jsonschema.ValidationError as e:
            print(f"Please check your {self.metadata_path} file: error while validating tenant metadata against schema")
            raise e

        return self.tenant

    def get_files(self) -> Dict:
        return dict(
            tenant=open(self.metadata_path, "rb"),
            **{
                path: open(path, "rb")
                for path in [
                    self.tenant["image_path"],
                    *list(map(lambda x: x["path"], self.tenant["intranet"]["files"])),
                ]
            },
        )

    def deploy(self, token: str) -> None:

        rich.print(f"Deploying {self.metadata_path} metadata to {self.post_url}")

        self.validate()

        files = self.get_files()

        rich.print(f"Deploying {len(files)} files along with the metadata")

        resp = requests.post(
            self.post_url,
            params={"token": token},
            files=self.get_files(),
        )

        if 400 <= resp.status_code < 500:
            raise Exception(
                f"Please check your {self.metadata_path} file: error while deploying tenant ({resp.status_code}): "
                + resp.text
            )

        if 500 <= resp.status_code:
            raise Exception(f"Server error while deploying tenant ({resp.status_code}): " + resp.text)

        rich.print(f"Tenant deployed successfully")
