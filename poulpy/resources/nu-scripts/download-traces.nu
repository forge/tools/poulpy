#!/usr/bin/env nu

def print-not-found [group_slug: string] {
    ^echo (ansi red) Trace not found for: $group_slug ! (ansi reset)
}

def save-trace [url: string, group_slug: string, dst_folder: path] {
    let output = $"($dst_folder)/($group_slug).xml"
    curl $url --output $output --silent
    ^echo (ansi green) Downloaded trace for: $group_slug ! (ansi reset)
}

def main [submission_definition_uri: string, dst_folder: path] {
    let submissions = (poulpy operator get-picked-submission $submission_definition_uri -f body | from json)

    let index = ($submission_definition_uri | str index-of "/root")
    let prefix = ($submission_definition_uri | str substring [0 $index] | str replace "/" "-")

    mkdir -v $dst_folder

    $submissions | par-each { |submission|
        if $submission.tracePresignedUrl != null {
            let groupSlug = ($submission.groupSlug | str replace $"($prefix)-" $"($prefix).")
            save-trace $submission.tracePresignedUrl $groupSlug $dst_folder
        } else {
            print-not-found $submission.groupSlug
        }
    }
}
