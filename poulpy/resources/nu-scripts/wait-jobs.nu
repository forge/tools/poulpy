#!/usr/bin/env nu

let states = [ "PREPARING" "PREPARED" "RUNNER_WAITING" "RUNNER_DISPATCHED" ]

def print-sleep [jobs: table] {
    let nb_jobs = ($jobs | length)
    ^echo (ansi blue) $nb_jobs jobs (if $nb_jobs == 1 { "is" } else { "are" }) still running ! (ansi reset)
    ^echo (ansi red) Sleeping for 30 seconds ... (ansi reset)
}

def print-finish [] {
    ^echo (ansi green) All jobs are done !  (ansi reset)
}

def get-running-jobs [activity_uri: string, submission_definition_uri: string] {
    let submission_ids = (
        poulpy operator get-picked-submission $submission_definition_uri -f body[].submissionId |
        from json
    )

    let jobs = (
        $submission_ids | par-each { |id|
            poulpy operator get-submission $activity_uri $id -f body.jobs | from json
        } | flatten
    )

    ($jobs | where $it.status in $states)
}

def main [activity_uri: string, submission_definition_uri: string] {
    mut jobs = get-running-jobs $activity_uri $submission_definition_uri

    while ($jobs | length) != 0 {
        print-sleep $jobs
        sleep 30sec
        $jobs = (get-running-jobs $activity_uri $submission_definition_uri)
    }

    print-finish
}
