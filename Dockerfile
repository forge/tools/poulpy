FROM python:3.10-slim

LABEL maintainer="Forge::dev <forge-dev@tickets.cri.epita.fr>"

ENV PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONPATH=/application_root \
    POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_CREATE=false \
    POETRY_VIRTUALENVS_OPTIONS_NO_PIP=true \
    POETRY_VIRTUALENVS_OPTIONS_NO_SETUPTOOLS=true

ADD . .

RUN DEBIAN_FRONTEND=noninteractive apt-get update \
    && apt install --no-install-recommends -y build-essential curl libmagic-dev \
    && rm -rf /var/lib/apt/lists/* \
    && pip install --no-cache-dir --upgrade pip \
    && pip install --no-cache-dir poetry \
    && poetry install --no-interaction --ansi --without dev

CMD ["poulpy"]
