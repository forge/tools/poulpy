import os
from pathlib import Path
import shutil
import subprocess

import pytest


@pytest.fixture(scope="class")
def init_preset_config():

    tmp_config_path = Path("/tmp/poulpy")
    config_path = Path(os.path.expanduser("~")) / ".poulpy"
    test_path = Path(__file__).parent.parent / "resources" / "tests_presets_config.json"

    shutil.rmtree(tmp_config_path, ignore_errors=True)
    shutil.copytree(config_path, tmp_config_path)
    shutil.copyfile(test_path, config_path / "presets.json")

    yield

    shutil.rmtree(config_path, ignore_errors=True)
    shutil.copytree(tmp_config_path, config_path)


@pytest.mark.usefixtures("init_preset_config")
class TestPresetsClass:
    def test_preset_with_no_format(self):
        # WHEN
        command: str = "poulpy test_no_args"
        process: subprocess.CompletedProcess = subprocess.run(
            command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True
        )

        # THEN
        assert process.returncode == 0
        assert process.stdout.decode("utf-8") == "toto\n"
        assert process.stderr.decode("utf-8") == ""

    def test_preset_too_much_args(self):
        # WHEN
        command: str = "poulpy test_no_args param1"
        process: subprocess.CompletedProcess = subprocess.run(
            command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True
        )

        # THEN
        assert process.returncode == 2
        assert process.stdout.decode("utf-8") == ""
        assert process.stderr.decode("utf-8") != ""

    def test_preset_description(self):
        # WHEN
        command: str = "poulpy test_no_args --help"
        process: subprocess.CompletedProcess = subprocess.run(
            command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True
        )

        # THEN
        assert process.returncode == 0
        assert "Basic test command" in process.stdout.decode("utf-8")
        assert process.stderr.decode("utf-8") == ""

    def test_preset_with_single_format(self):
        # WHEN
        command: str = "poulpy test_single_format --param example"
        process: subprocess.CompletedProcess = subprocess.run(
            command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True
        )

        # THEN
        assert process.returncode == 0
        assert process.stdout.decode("utf-8") == "example\n"
        assert process.stderr.decode("utf-8") == ""

    def test_preset_with_multiple_format(self):
        # WHEN
        command: str = "poulpy test_multiple_format --param1 example1 --param2 example2"
        process: subprocess.CompletedProcess = subprocess.run(
            command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True
        )

        # THEN
        assert process.returncode == 0
        assert process.stdout.decode("utf-8") == "example1 example2\n"
        assert process.stderr.decode("utf-8") == ""

    def test_preset_with_missing_arg(self):
        # WHEN
        command: str = "poulpy test_missing_arg --param example"
        process: subprocess.CompletedProcess = subprocess.run(
            command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True
        )

        # THEN
        assert process.returncode == 1
        assert process.stdout.decode("utf-8") == ""
        assert process.stderr.decode("utf-8") != ""

    def test_preset_with_escaped_arg(self):
        # WHEN
        command: str = "poulpy test_escaped_format --param example"
        process: subprocess.CompletedProcess = subprocess.run(
            command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True
        )

        # THEN
        assert process.returncode == 0
        assert process.stdout.decode("utf-8") == "example {param2}\n"
        assert process.stderr.decode("utf-8") == ""

    def test_preset_with_unused_arg(self):
        # WHEN
        command: str = "poulpy test_unused_arg --param example"
        process: subprocess.CompletedProcess = subprocess.run(
            command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True
        )

        # THEN
        assert process.returncode == 0
        assert process.stdout.decode("utf-8") == "toto\n"
        assert process.stderr.decode("utf-8") == ""

    @pytest.mark.skipif(os.environ.get("CI") is not None, reason="We cannot fetch the authorization token from the CI")
    def test_preset_complex(self):
        # WHEN
        command: str = "poulpy test_complex --submission_uri epita-prepa-acdc/prog-101-p-01-2027/root/ACDC-PROG-101-P-01/ACDC-submit"
        process: subprocess.CompletedProcess = subprocess.run(
            command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True
        )

        # THEN
        assert process.returncode == 0
        assert process.stdout.decode("utf-8") != ""
        assert process.stderr.decode("utf-8") == ""
