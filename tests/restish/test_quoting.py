from poulpy.restish import Restish


def test_no_quoting1():
    args = ["api§", "method#"]
    assert Restish._quote_args(args) == args


def test_no_quoting2():
    args = ["api§", "--help"]
    assert Restish._quote_args(args) == args


def test_no_quoting3():
    args = ["--help"]
    assert Restish._quote_args(args) == args


def test_no_quoting4():
    args = ["-@", "-£", "-%", "--tot*", "api§", "method#"]
    assert Restish._quote_args(args) == args


def test_no_quoting5():
    args = ["-@", "-£", "-%", "--tot*", "api§"]
    assert Restish._quote_args(args) == args


def test_no_quoting6():
    args = ["api§", "method#", "-@", "-£", "-%", "--tot*"]
    assert Restish._quote_args(args) == args


def test_no_quoting7():
    args = ["api§", "-@", "-£", "-%", "--tot*", "method#"]
    assert Restish._quote_args(args) == args


def test_no_quoting8():
    args = ["api§", "-@", "-£", "-%", "--tot*", "-o", "js¤n", "method#"]
    assert Restish._quote_args(args) == ["api§", "-@", "-£", "-%", "--tot*", "-o", "js¤n", "method#"]


def test_quoting1():
    args = ["api§", "method#", "tenant-slug/activity-slug"]
    assert Restish._quote_args(args) == ["api§", "method#", "tenant-slug%2Factivity-slug"]


def test_quoting2():
    args = ["api§", "-@", "-£", "-%", "--tot*", "method#", "tenant-slug/activity-slug"]
    assert Restish._quote_args(args) == ["api§", "-@", "-£", "-%", "--tot*", "method#", "tenant-slug%2Factivity-slug"]


def test_quoting3():
    args = ["api§", "-@", "-£", "-%", "--tot*", "-o", "js¤n", "method#", "tenant-slug/activity-slug"]
    assert Restish._quote_args(args) == [
        "api§",
        "-@",
        "-£",
        "-%",
        "--tot*",
        "-o",
        "js¤n",
        "method#",
        "tenant-slug%2Factivity-slug",
    ]


def test_quoting4():
    args = ["api§", "-@", "-£", "-%", "--tot*", "method#", "tenant-slug/activity-slug", "-o", "js¤n"]
    assert Restish._quote_args(args) == [
        "api§",
        "-@",
        "-£",
        "-%",
        "--tot*",
        "method#",
        "tenant-slug%2Factivity-slug",
        "-o",
        "js¤n",
    ]


def test_quoting5():
    args = ["api§", "method#", "tenant-slug activity-slug"]
    assert Restish._quote_args(args) == ["api§", "method#", "tenant-slug%20activity-slug"]


def test_quoting6():
    args = ["api§", "method#", "tenant-slug!activity-slug"]
    assert Restish._quote_args(args) == ["api§", "method#", "tenant-slug%21activity-slug"]


def test_quoting7():
    args = ["api§", "method#", "tenant-slug{activity-slug"]
    assert Restish._quote_args(args) == ["api§", "method#", "tenant-slug%7Bactivity-slug"]


def test_quoting8():
    args = ["api§", "method#", "-H", "application/json", "tenant-slug|activity-slug"]
    assert Restish._quote_args(args) == ["api§", "method#", "-H", "application/json", "tenant-slug%7Cactivity-slug"]


def test_quoting_option_value1():
    args = ["api§", "method#", "-f", "body.results[]{author,jobId}", "tenant-slug activity-slug"]
    assert Restish._quote_args(args) == [
        "api§",
        "method#",
        "-f",
        "body.results[]{author,jobId}",
        "tenant-slug%20activity-slug",
    ]


def test_quoting_option_value2():
    args = ["api§", "method#", "tenant-slug!activity-slug"]
    assert Restish._quote_args(args) == ["api§", "method#", "tenant-slug%21activity-slug"]
