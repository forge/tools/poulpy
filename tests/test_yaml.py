import os
from pathlib import Path
from poulpy.intranet import yaml


def test_full():
    test_base = Path(__file__).parent
    previous_cwd = Path.cwd()
    os.chdir(test_base)

    # WHEN
    with open(test_base / "resources" / "include.yaml") as file:
        data = yaml.load(file)

    os.chdir(previous_cwd)

    # THEN
    assert data == {
        "test": 1,
        "basic": {
            "test1": "a",
            "test2": [
                3.5,
            ],
        },
        "nested": {
            "nested": {
                "test1": "a",
                "test2": [
                    3.5,
                ],
            },
        },
        "json": {
            "test1": "a",
            "test2": [
                3.5,
            ],
        },
        "toml": {
            "test1": "a",
            "test2": [
                3.5,
            ],
        },
    }
