import os
import subprocess

import pytest

from poulpy.pipeline import Pipeline
from poulpy.config import Config


def test_pipeline_run_init():
    # WHEN
    Pipeline("echo 0")

    # THEN
    assert Config.nushell_binary.exists()
    assert Config.restish_binary.exists()

    assert Config.presets_config.exists()
    assert Config.restish_config.exists()
    assert Config.scripts_folder.exists()


def test_pipeline_run_echo():
    # GIVEN
    pipeline = Pipeline("echo 0")

    # WHEN
    process: subprocess.CompletedProcess = pipeline.run("prod", stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    # THEN
    assert process.returncode == 0
    assert process.stdout.decode("utf-8") == "0\n"
    assert process.stderr.decode("utf-8") == ""


@pytest.mark.skipif(os.environ.get("CI") is not None, reason="We cannot fetch the authorization token from the CI")
def test_pipeline_run_restish_basic():
    # GIVEN
    pipeline = Pipeline("operator --help")

    # WHEN
    process: subprocess.CompletedProcess = pipeline.run("prod", stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    # THEN
    assert process.returncode == 0, process.stderr.decode("utf-8")
    assert process.args == [str(pipeline.nushell_path), "-c", "poulpy operator -p prod --help"], process.stderr.decode(
        "utf-8"
    )


@pytest.mark.skipif(os.environ.get("CI") is not None, reason="We cannot fetch the authorization token from the CI")
def test_pipeline_run_restish_basic_other_profile():
    # GIVEN
    pipeline = Pipeline("operator --help")

    # WHEN
    process: subprocess.CompletedProcess = pipeline.run("staging", stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    # THEN
    assert process.returncode == 0, process.stderr.decode("utf-8")
    assert process.args == [
        str(pipeline.nushell_path),
        "-c",
        "poulpy operator -p staging --help",
    ], process.stderr.decode("utf-8")
