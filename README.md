# Poulpy

An easy-to-use multi-fonction CLI tool for The Forge users

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install poulpy.

```bash
pip install poulpy --extra-index-url https://gitlab.cri.epita.fr/api/v4/projects/8706/packages/pypi/simple
```

## Configuration

Once installed, poulpy configuration is generated in the `~/.poulpy` directory.

The configuration file `~/.poulpy/restish.json`, defines the APIs on which poulpy can make http request.

The default configuration file only specify forge services APIs but it can be modified to suit your needs.


The syntax of this file is the following:

```json
{
    // Define the API name used by Poulpy
    "API_NAME": {
        // Define the API base URL
        "base": "http://localhost:8000",
        // Define where the related OpenApi specifications files are located (Can be an URL or a local path)
        "spec_files": ["http://localhost:8000/q/openapi"]
    },
}
```

Then you can call your API endpoints with the following command:

```bash
poulpy API_NAME my_super_endpoint
```

**Warning**
The OpenApi spec files must defined endpoints with unique non-empty `operationId` fields in order to be parsed and used properly.

**Note**
As Poulpy use restish to make http requests, you can check restish [documentation](https://restish.readthedocs.io/en/latest/) to know how to personalize requests.

## Usage

Poulpy is the CLI tool used at the Forge::dev, it can contain anything you want.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

### Dev environment

You need Poetry to start the dev environment.
follow the following link installation steps if it's not installed yet: [Poetry installation](https://python-poetry.org/docs/master/#installing-with-the-official-installer)

Install the packages needed by Poulpy.

```shell
poetry install
```

Enter the poetry env

```shell
poetry shell
```

### Create a new group

Create a new folder in the folder `poulpy`, then to connect it with `click` follow the following steps:

- Add your new command in your newly created file and folder

```py
import click

@click.command()
def awesome_command():
    print("Awesome stuff goes here!")
```

- In `poulpy/__init__.py` set your new group and add your commands to it.

```py
# Import your new commands
from .my_new_cli_group.new_file import awesome_command

...

@main.group()
def my_new_cli_group():
    pass

my_new_cli_group.add_command(awesome_command)
```

Then you can use it like this:

```sh
$ poulpy my-new-cli-group awesome-command
Awesome stuff goes here!
```

## License

[MIT](https://choosealicense.com/licenses/mit/)
